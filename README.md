# Image Download

To download images from bing using keywords


Pre-requisites
- compatible chromedriver for your chrome browser
- python3 and above

Steps
- Edit the labels.txt with labels of images to be downloaded (check once the result for labels in "https://www.bing.com/images/")

- Batch Run using below command

  python downloader.py --limit 3 --download --chromedriver "driverpath" --keywords_from_file labels.txt
  
- For running a single label

  python downloader.py --search "babies" --limit 3 --download --chromedriver "driverpath"